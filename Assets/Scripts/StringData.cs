public abstract class StringData
{
        public const string Ball = "Ball";
        public const string Wall = "Wall";
        public const string Player = "Player";
        public const string PlayerOne = "Player One";
        public const string PlayerTwo = "Player Two";
        public const string PlayerOneArea = "Player One Area";
        public const string PlayerTwoArea = "Player Two Area";
        public const string LeftWall = "Left Wall";
        public const string RightWall = "Right Wall";
}