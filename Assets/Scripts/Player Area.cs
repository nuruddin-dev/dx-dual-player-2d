using UnityEngine;

public class PlayerArea : MonoBehaviour
{
    public GameObject player;
    private Camera _mainCamera;
    private float _screenAtMostWidthSize;
    private bool _isMouseEntered;
    private Vector3 _mousePosition; // Current mouse position (PC only)
    
    // Start is called before the first frame update
    void Start()
    {
        if (Camera.main == null) return;
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // PC Mouse Interaction
        _mousePosition = Input.mousePosition;
        
        if (_isMouseEntered)
        {
            var mousePosition = _mainCamera.ScreenToWorldPoint(_mousePosition);
            var playerPositionX = mousePosition.x;
            var playerPositionY = player.transform.position.y;
            player.transform.position = new Vector2(playerPositionX, playerPositionY);
        }
    }

    private void OnMouseEnter()
    {
        _isMouseEntered = true;
    }

    private void OnMouseExit()
    {
        _isMouseEntered = false;
    }
}
