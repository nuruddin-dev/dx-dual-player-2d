using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameManager gameManager;
    private Rigidbody2D _rigidBody2D;
    private Vector2 _currentDirection;
    public float shootForce = 2000f;

    private const float Angle = 45f;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
        var directionX = Mathf.Clamp((Angle / 90f), -1f, 1f);
        var directionY = 1 - Mathf.Clamp(Mathf.Abs(directionX), 0f, 1f);
        AddForceToRigidBody(directionX, directionY);
    }
    
    private void OnBecameInvisible()
    {
        gameManager.CreateNewBall();
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(StringData.Wall))
        {
            _rigidBody2D.velocity = Vector2.zero;
            AddForceToRigidBody( -_currentDirection.x, _currentDirection.y);
        }

        if (other.gameObject.CompareTag(StringData.Player))
        {
            AddForceToRigidBody( _currentDirection.x, -_currentDirection.y);
        }
        
    }

    private void AddForceToRigidBody(float newDirectionX, float newDirectionY)
    {
        var newDirection = new Vector2(newDirectionX, newDirectionY);
        _rigidBody2D.AddForce(newDirection * shootForce);
        // Debug.Log($"total force: {_rigidBody2D.totalForce}");
        StartCoroutine(WaitAndSyncNewDirection(newDirection));
    }
    
    private IEnumerator WaitAndSyncNewDirection(Vector2 newDirection)
    {
        yield return new WaitForSeconds(.01f);
        _currentDirection = newDirection;
    }
}
