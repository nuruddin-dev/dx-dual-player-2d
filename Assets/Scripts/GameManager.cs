using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    private Camera _mainCamera;
    private float _screenWidth;
    private float _screenHeight;
    
    public GameObject playerUnit;
    public GameObject ball;
    public GameObject wallUnit;
    public GameObject areaUnit;

    private GameObject _playerOne;
    private GameObject _playerTwo;
    private GameObject _playerOneArea;
    private GameObject _playerTwoArea;
    private GameObject _leftWall;
    private GameObject _rightWall;
    private GameObject _newBall;
    
    void Start()
    {
        GetScreenHeightWidth();
        CreatePlayers();
        CreatePlayerAreas();
        CreateWalls();
        CreateNewBall();
    }

    private void GetScreenHeightWidth()
    {
        if (Camera.main == null) return;
        _mainCamera = Camera.main;
        _screenHeight = _mainCamera.orthographicSize * 2f;
        _screenWidth = _screenHeight * _mainCamera.aspect;
    }

    private void CreatePlayers()
    {
        var playerScaleX = _screenWidth / 3;
        var playerScaleY = 1f;
        
        _playerOne = Instantiate(playerUnit, Vector2.zero, Quaternion.identity);
        _playerOne.name = StringData.PlayerOne;
        _playerOne.transform.localScale = new Vector2(playerScaleX, playerScaleY);
        _playerOne.transform.position = new Vector2(0f, -_screenHeight/2);
        
        _playerTwo = Instantiate(playerUnit, Vector2.zero, Quaternion.identity);
        _playerTwo.transform.localScale = new Vector2(playerScaleX, playerScaleY);
        _playerTwo.transform.position = new Vector2(0f, _screenHeight/2);
        _playerTwo.name = StringData.PlayerTwo;
    }

    private void CreatePlayerAreas()
    {
        _playerOneArea = Instantiate(areaUnit, Vector2.zero, Quaternion.identity);
        _playerOneArea.name = StringData.PlayerOneArea;
        _playerOneArea.transform.localScale = new Vector2(_screenWidth, _screenHeight / 2);
        _playerOneArea.transform.position = new Vector2(0f, -_screenHeight / 4);
        _playerOneArea.AddComponent<PlayerArea>().player = _playerOne;
        
        _playerTwoArea = Instantiate(areaUnit, Vector2.zero, Quaternion.identity);
        _playerTwoArea.name = StringData.PlayerTwoArea;
        _playerTwoArea.transform.localScale = new Vector2(_screenWidth, _screenHeight / 2);
        _playerTwoArea.transform.position = new Vector2(0f, _screenHeight / 4);
        _playerTwoArea.AddComponent<PlayerArea>().player = _playerTwo;
    }
    
    private void CreateWalls()
    {
        _leftWall = Instantiate(wallUnit, Vector2.zero, Quaternion.identity);
        _leftWall.name = StringData.LeftWall;
        _leftWall.transform.localScale = new Vector2(1f, _screenHeight);
        _leftWall.transform.position = new Vector2(-_screenWidth / 2, 0f);
        
        _rightWall = Instantiate(wallUnit, Vector2.zero, Quaternion.identity);
        _rightWall.name = StringData.RightWall;
        _rightWall.transform.localScale = new Vector2(1f, _screenHeight);
        _rightWall.transform.position = new Vector2(_screenWidth / 2, 0f);
    }
    
    public void CreateNewBall()
    {
        _newBall = Instantiate(ball, Vector2.zero, Quaternion.identity);
        _newBall.name = StringData.Ball;
        _newBall.GetComponent<Ball>().gameManager = this;
    }

    private void OnDestroy()
    {
        Destroy(_newBall);
    }
}
